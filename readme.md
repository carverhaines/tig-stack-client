## About
Telegraf client for servers that record metric and report it back to the InfluxDB server. This can be installed as a package directly on the system (ideal if docker not installed on the server) or run as a docker client.

---

## Run Client in Docker (RECOMMENDED)
For clients that have docker installed and running on the machine.
#### 1.  Clone this repo
```
git clone https://gitlab.com/carverhaines/tig-stack-client.git
cd tig-stack-client
```
#### 2.  Setup config files
1. Use the example telegraf files from this repo, it has common plugins activated (CPU, RAM, Disk, Network, Docker), along with an environment variables file defining the server IP and client hostname. Using the pre-configured `telegraf.conf` from this repo, you only need to copy the customized `config.env`, `.env`, and your `influxdb.pem` files to different servers.
```
cp telegraf.conf.example telegraf.conf
cp config.env.example config.env
cp .env.example .env
vim config.env    #edit parameters as necessary
vim .env          #use same hostname as config.env
```

2. Copy your SSL/TLS certificate file to ssl/influxdb.pem
```
vim ssl/influxdb.pem
```

3.  Alternatively generate a default telegraf config file (most things are commented out) and enable settings/plugins you desire:
```
docker run --rm telegraf telegraf config > telegraf.conf
```

#### 3.  Run the telegraf container
```
docker-compose up -d
```

---

## Install Client directly on the system (Alternate)

#### How to install telegraf for different distributions:
https://docs.influxdata.com/telegraf/latest/introduction/installation/

Info for installing on FreeNAS/FreeBSD: https://www.reddit.com/r/freenas/comments/81t2bw/can_i_install_telegraf_on_my_freenas_host/

#### There are two options for a telegraf config file (/etc/telegraf/telegraf.conf)
1.  Generate a default telegraf config file (most things are commented out):
```
telegraf config > /etc/telegraf/telegraf.conf
```

or

2.  Download the example config from this repo, it has common plugins activated (CPU, RAM, Disk, Network, Docker), along with the `telegraf` environment variables file.
```
wget -O /etc/default/telegraf https://gitlab.com/carverhaines/tig-stack-client/raw/master/telegraf.example
wget -O /etc/telegraf/telegraf.conf https://gitlab.com/carverhaines/tig-stack-client/raw/master/telegraf.conf.example
```

Any variables in the /etc/telegraf/telegraf file will automatically used in the /etc/telegraf/telegraf.conf

---
